 ### Literature about LRP applied to NLP

1. Samek, R., Wiegand, T.,  Harbecke, D.,& K.-R. (2017). [***EXPLAINABLE ARTIFICIAL INTELLIGENCE:UNDERSTANDING,VISUALIZING ANDINTERPRETING DEEP LEARNING MODELS***](https://arxiv.org/pdf/1708.08296.pdf). arXiv preprint arXiv:1708.08296.(1)

2. Arras, L., Montavon, G.,  Müller, k., & Samek, W. (2017). [***Explaining Recurrent Neural Network Predictions in Sentiment Analysis***](https://arxiv.org/pdf/1706.07206.pdf). arXiv preprint arXiv:1706.07206.(2)

3. Schwarzenberg, R., Hubner, M.,  Harbecke, D., Hennig, L.,& Alt, C. (2019). [***Layerwise Relevance Visualization in Convolutional Text Graph
Classifiers***](https://arxiv.org/pdf/1909.10911.pdf). arXiv preprint arXiv:1909.10911.(3)


### Literature about Relation Extraction task
 
1. Kumar,S. [**A Survey of Deep Learning Methods for Relation Extraction** (5)
](https://arxiv.org/pdf/1705.03645.pdf)

2. Shahbazi, H., Fern, X., Ghaeini, R., Tadepalli, P. (2020).[***Relation Extraction with Explanation***()
](https://www.aclweb.org/anthology/2020.acl-main.579.pdf)

3. Jurafsky, D., Martin, J., H. (2020). Information Extraction.  Speech and Language Processing: An Introduction to Natural Language Processing,
Computational Linguistics, and Speech Recognition (Third Edition draft, pp.332-354).[***Speech and Language Processing***
](https://web.stanford.edu/~jurafsky/slp3/ed3book.pdf)(4)

4. D. Zeng, K. Liu, S. Lai, G. Zhou, and J. Zhao, “Relation classifi-cation via convolutional deep neural network,” inProceedings ofCOLING 2014, the 25th International Conference on ComputationalLinguistics: Technical Papers, Dublin, Ireland: Dublin City Uni-versity and Association for Computational Linguistics, Aug. 2014,pp. 2335–2344. [***Relation Classification via Convolutional Deep Neural Network***](https://www.aclweb.org/anthology/C14-1220.pdf)

**SEMEval 2010 Task 8 papers**
1. Liu, Yang, et al. ["A dependency-based neural network for relation classification."](https://arxiv.org/pdf/1507.04646v1.pdf) arXiv preprint arXiv:1507.04646 (2015).
2. Zhang, Dongxu, and Dong Wang. ["Relation classification via recurrent neural network."](https://arxiv.org/pdf/1508.01006.pdf) arXiv preprint arXiv:1508.01006 (2015).


**Explainability**
1. Li, Jiwei, et al. ["Visualizing and understanding neural models in nlp."](https://arxiv.org/pdf/1506.01066.pdf) arXiv preprint arXiv:1506.01066 (2015).

