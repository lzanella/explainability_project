## Literature related to LRP and its applications to NLP 
---
### Literature about LRP
---
- Bach, S., Binder, A., Montavon, G., Klauschen, F., Müller, K. R., & Samek, W. (2015). [***On pixel-wise explanations for non-linear classifier decisions by layer-wise relevance propagation***](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0130140). PloS one, 10(7), e0130140.

- Samek, W., Montavon, G., Binder, A., Lapuschkin, S., & Müller, K. R. (2016). [***Interpreting the predictions of complex ml models by layer-wise relevance propagation***](https://arxiv.org/pdf/1611.08191). arXiv preprint arXiv:1611.08191.

- Montavon, G., Samek, W., & Müller, K. R. (2018). [***Methods for interpreting and understanding deep neural networks***](https://www.sciencedirect.com/science/article/pii/S1051200417302385). Digital Signal Processing, 73, 1-15.

- Lapuschkin, S. (2019). [***Opening the machine learning black box with Layer-wise Relevance Propagation***](https://d-nb.info/1177139251/34).

---
---
### Literature about NLP
---
- Hafiane, W., Legrand, J., Toussaint, Y., & Coulet, A. (2020). [***Experiments on transfer learning architectures for biomedical relation extraction***](https://arxiv.org/pdf/2011.12380). arXiv preprint arXiv:2011.12380.

- Khattak, F. K., Jeblee, S., Pou-Prom, C., Abdalla, M., Meaney, C., & Rudzicz, F. (2019). [***A survey of word embeddings for clinical text***](https://www.sciencedirect.com/science/article/pii/S2590177X19300563). Journal of Biomedical Informatics: X, 4, 100057.
---
---
### Literature about Deep Learning
---
- Kipf, T. N., & Welling, M. (2016). [***Semi-supervised classification with graph convolutional networks***](https://arxiv.org/pdf/1609.02907.pdf?source=post_page---------------------------). arXiv preprint arXiv:1609.02907.

- Wu, Z., Pan, S., Chen, F., Long, G., Zhang, C., & Philip, S. Y. (2020). [***A comprehensive survey on graph neural networks. IEEE transactions on neural networks and learning systems***](https://ieeexplore.ieee.org/iel7/5962385/9312808/09046288.pdf?casa_token=Yk75PHxTbxwAAAAA:itAFsL4qhg7W5KYhOt4UOfVZO68qhnx9I6Fy3FULdEGfNksicakcSqh6hWNvLXNNDBiddeVQ).
---
---
### Literature about LRP applied to NLP
---
- Danilevsky, M., Qian, K., Aharonov, R., Katsis, Y., Kawas, B., & Sen, P. (2020). [***A survey of the state of explainable AI for natural language processing***](https://arxiv.org/pdf/2010.00711). arXiv preprint arXiv:2010.00711.

- Hu, J. (2018). [***Explainable Deep Learning for Natural Language Processing***](https://www.diva-portal.org/smash/get/diva2:1335846/FULLTEXT01.pdf).

- Horn, F., Arras, L., Montavon, G., Müller, K. R., & Samek, W. (2017). [***Exploring text datasets by visualizing relevant words***](https://arxiv.org/pdf/1707.05261). arXiv preprint arXiv:1707.05261.

- Schwarzenberg, R., Hübner, M., Harbecke, D., Alt, C., & Hennig, L. (2019). [***Layerwise Relevance Visualization in Convolutional Text Graph Classifiers***](https://arxiv.org/pdf/1909.10911). arXiv preprint arXiv:1909.10911.

- [***Heatmapping.org***](http://heatmapping.org): website that aims to regroup publications and software produced as part of a joint project at **Fraunhofer HHI**, **TU Berlin** and **SUTD Singapore** on developing new method to understand nonlinear predictions of state-of-the-art machine learning models.
- Arras, L., Montavon, G.,  Müller, k., & Samek, W. (2017). [***Explaining Recurrent Neural Network Predictions in Sentiment Analysis***](https://arxiv.org/pdf/1706.07206.pdf). arXiv preprint arXiv:1706.07206.


- Schwarzenberg, R., Hubner, M.,  Harbecke, D., Hennig, L.,& Alt, C. (2019). [***Layerwise Relevance Visualization in Convolutional Text Graph
Classifiers***](https://arxiv.org/pdf/1909.10911.pdf). arXiv preprint arXiv:1909.10911.
---

